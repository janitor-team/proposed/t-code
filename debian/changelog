t-code (2:2.3.1-9) unstable; urgency=medium

  * d/control: Prefer emacs-nox and emacs instead of emacs25 (Closes: #952145)
    Thanks to Tatsuya Kinoshita <tats@debian.org>.
  * d/control: Bump debhelper compatibility level to 12
    and use debhelper-compat instead of d/compat.
  * d/control: set Rules-Requires-Root: as no.
  * bump up Standards-Version 4.5.0.

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Mon, 13 Apr 2020 21:36:25 +0900

t-code (2:2.3.1-8) unstable; urgency=medium

  * d/p/use_string-to-number_instead_of_string-to-int.patch: new patch.
    - use string-to-number instead of string-to-int deprecated in emacs26
      (Closes: #916869)
  * d/p/series: remove duplicated entry
  * Update Standards-Version to 4.3.0

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Thu, 27 Dec 2018 19:18:17 +0900

t-code (2:2.3.1-7) unstable; urgency=medium

  * Move Vcs-* to salsa.debian.org.
  * Update Standards-Version to 4.1.3.
  * Bump debhelper compatibility level to 11.
  * eliminate lintian warning: file-contains-trailing-whitespace
  * eliminate lintian warning: insecure-copyright-format-uri
  * re-apply d/p/add_skkinput3_load_path.diff

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Wed, 30 May 2018 18:25:19 +0900

t-code (2:2.3.1-6) unstable; urgency=medium

  * d/rules: do not build parallel due to invite race condition.
  * marked as multi-arch: foreign.
  * bump up Standards-Version 4.0.0.

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Sat, 19 Aug 2017 20:53:37 +0900

t-code (2:2.3.1-5) unstable; urgency=medium

  * Bump {build-,}deps emacs24 -> emacs25 (Closes: #870667).
    - thanks to Sean Whitton <spwhitton@spwhitton.name>
  * d/rules, d/compat, d/control: use dh10.

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Fri, 04 Aug 2017 21:02:52 +0900

t-code (2:2.3.1-4) unstable; urgency=medium

  * debian/control
    - set myself as the maintainer with ex-maintainer's permission.
      thanks to NOSHIRO Shigeo <noshiro@debian.org> for previous works.
    - eliminate lintian warning: vcs-field-uses-insecure-uri
    - remove duplicate dependency.
    - remove transitional dependency.
    - bump up Standards-Version 3.9.8.
  * debian/copyright
    - eliminate lintian warning: dep5-copyright-license-name-not-unique

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Sun, 31 Jul 2016 14:30:46 +0900

t-code (2:2.3.1-3.5) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/control
    - depends emacs24 instead of emacs23,
      emacs23 has been removed from jessie (Closes: #768727).
  * fix missing copyright file after upgrade (Closes: #768239).
    - debian/t-code.maintscript: new file.
    - debian/control: add Pre-Depends: dpkg (>= 1.17.5).

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Sat, 15 Nov 2014 14:57:57 +0900

t-code (2:2.3.1-3.4) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/control: fix Description (Closes: #742819)

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Wed, 02 Apr 2014 20:43:39 +0900

t-code (2:2.3.1-3.3) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/control
    - add dh-autoreconf to Build-Depends:
    - update emacsen in Depends: and Build-Depends:
    - split t-code package into t-code and t-code-common.
  * debian/copyright: convert to DEP-5.
  * debian/*.install: convert from debian/dirs.
  * debian/t-code-common.docs: convert from debian/docs.
  * debian/t-code.emacsen-*: rename from debian/emacsen-*.
  * debian/t-code-common.examples: rename from debian/examples.
  * debian/t-code.info: rename from debian/info.
  * debian/patches/*: split *.diff.gz.
  * debian/rules, debian/compat: use dh9.
  * debian/source/format: set 3.0 (quilt).
  * debian/t-code-common.doc-base: new file.
  * debian/watch: new file.
  * bump up Standards-Version 3.9.5

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Tue, 11 Mar 2014 20:49:02 +0900

t-code (2:2.3.1-3.2) unstable; urgency=low

  * Non-maintainer upload.

  [ Eric Dorland ]
  * debian/control: Drop unnecessary build-deps on autoconf and
    automake1.4. (Closes: #724010)

 -- Eric Dorland <eric@debian.org>  Sun, 12 Jan 2014 19:31:28 -0500

t-code (2:2.3.1-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Rebuild with newer debhelper to get rid of install-info calls in
    maintainer scripts. (Closes: #708499)

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 10 Aug 2013 21:37:50 +0200

t-code (2:2.3.1-3) unstable; urgency=low

  * FIX tc-sysdep.el.
    - Fix incompatibilities emacs22.
  * FIX lisp/Makefile.am
    - Set directory /usr/share/t-code
  * debian/control:
    - Move `Homepage:' from Description to the header.
    - Set Standards-Version to 3.7.3.

 -- NOSHIRO Shigeo <noshiro@debian.org>  Tue, 03 Feb 2009 10:09:25 +0900

t-code (2:2.3.1-2.1) unstable; urgency=low

  * Non-maintainer upload, complied with the maintainer's request,
    closes: Bug#453458
  * Fix lintian error and warnings, closes: Bug#453307
    - Move debhelper from Build-Depends-Indep to Build-Depends.
    - Bump up debhelper version to 5.
    - Don't use DH_COMPAT in debian/rules, use debian/compat instead.
    - Don't ignore an error of make distclean in debian/rules.
    - Update FSF address in debian/copyright.
    - Bump up Standerd-Version to 3.7.2.
  * debian/rules:
    - Fix FTBFS when /bin/sh is dash, closes: Bug#438531
    - Use binary-indep instead of binary-arch, closes: Bug#438530
  * debian/emacsen-startup:
    - Use debian-pkg-add-load-path-item to set load-path, closes: Bug438532
    - Fix that startup fails if package is removed but not purged,
      closes: Bug#438533
  * debian/control:
    - Prefer emacs and emacs22, closes: Bug#433982
    - Revise description to mention tc2, emacsen and the upstream site,
      closes: Bug#453427
  * debian/emacsen-install: Create symlinks for *.el files, rather than
    copying *.el files and then removing them, closes: Bug#438534

 -- Tatsuya Kinoshita <tats@debian.org>  Sat, 01 Dec 2007 07:19:58 +0900

t-code (2:2.3.1-2) unstable; urgency=low

  * FIX tcode-site-data-directory in tc-pre.el

 -- NOSHIRO Shigeo <noshiro@debian.org>  Tue,  1 Jul 2003 13:31:50 +0900

t-code (2:2.3.1-1) unstable; urgency=low

  * New upstream release

 -- NOSHIRO Shigeo <noshiro@debian.org>  Mon, 23 Jun 2003 19:02:56 +0900

t-code (2:2.3-2) unstable; urgency=low

  * FIX tcode-site-data-directory in tc-pre.el, closes: Bug#191765
  * remove Depends: emacs20-dl (it was removed from Debian tree)
     closes: #190830

 -- NOSHIRO Shigeo <noshiro@debian.org>  Sun,  4 May 2003 02:10:19 +0900

t-code (2:2.3-1) unstable; urgency=low

  * New upstream release (Official release.)

 -- NOSHIRO Shigeo <noshiro@debian.org>  Thu, 17 Apr 2003 19:05:17 +0900

t-code (2:2.2.1+2.3pre19-1) unstable; urgency=low

  * New upstream release

 -- NOSHIRO Shigeo <noshiro@debian.org>  Mon, 17 Mar 2003 15:24:18 +0900

t-code (2:2.2.1+2.3pre17-0.1) unstable; urgency=low

  * Non Maintainer.
  * New upstream realse.

 -- akira yamada <akira@debian.org>  Thu, 13 Feb 2003 16:37:03 +0900

t-code (2:2.2.1+2.3pre9.1-1) unstable; urgency=low

  * New upstream releaes
  * Change architecture: all, closes: #146753

 -- NOSHIRO Shigeo <noshiro@debian.org>  Sat, 14 Sep 2002 14:33:35 +0900

t-code (2:2.2.1+2.3pre5.1-5) unstable; urgency=low

  * local test

 -- NOSHIRO Shigeo <noshiro@debian.org>  Thu, 22 Aug 2002 16:03:10 +0900

t-code (2:2.2.1+2.3pre5.1-4) unstable; urgency=low

  * FIX emacen-install script check MULE functions, closes: Bug#146755

 -- NOSHIRO Shigeo <noshiro@debian.org>  Mon, 13 May 2002 15:09:34 +0900

t-code (2:2.2.1+2.3pre5.1-3) unstable; urgency=medium

  * Add to Depends (xemacs21-gnome-mule-canna-wnn | xemacs21-gnome-mule)
  * FIX mk-bdic,mk-mzdic: fails compiling dictionary, closes: #144869

 -- NOSHIRO Shigeo <noshiro@debian.org>  Thu,  9 May 2002 13:53:32 +0900

t-code (2:2.2.1+2.3pre5.1-2) unstable; urgency=low

  * Add to Build-Depends.
  * FIX emacs-install.

 -- NOSHIRO Shigeo <noshiro@debian.org>  Mon,  6 May 2002 00:11:44 +0900

t-code (2:2.2.1+2.3pre5.1-1) unstable; urgency=medium

  * New upstream release
  * FIX Build-Depneds: are wrong or missing, closes: Bug#145582

 -- NOSHIRO Shigeo <noshiro@debian.org>  Fri,  3 May 2002 17:04:47 +0900

t-code (2:2.2.1+2.3pre5-1) unstable; urgency=low

  * New upstream release

 -- NOSHIRO Shigeo <noshiro@debian.org>  Sat, 27 Apr 2002 19:35:27 +0900

t-code (2:2.2.1+2.3pre4-1) unstable; urgency=low

  * New upstream release.
    - FIX build problem(on merulo), closes: Bug#118380

 -- NOSHIRO Shigeo <noshiro@debian.org>  Thu, 28 Mar 2002 16:18:29 +0900

t-code (2:2.2.1+2.3pre3-1) unstable; urgency=low

  * New upstream release

 -- NOSHIRO Shigeo <noshiro@debian.org>  Mon, 25 Mar 2002 10:47:01 +0900

t-code (2:2.2.1+2.3pre2-2) unstable; urgency=low

  * fix tcode-site-data-directory in tc-site.el

 -- NOSHIRO Shigeo <noshiro@debian.org>  Sun, 24 Mar 2002 00:30:04 +0900

t-code (2:2.2.1+2.3pre2-1) unstable; urgency=low

  * New upstream release
  * add (require 'tc-setup) in lisp/tc-sysdep.el

 -- NOSHIRO Shigeo <noshiro@debian.org>  Sat, 23 Mar 2002 11:09:13 +0900

t-code (2:2.2.1-3) unstable; urgency=low

  * remove (require 'tc-setup) debian/emacsen-startup

 -- NOSHIRO Shigeo <noshiro@debian.org>  Wed, 13 Mar 2002 14:16:46 +0900

t-code (2:2.2.1-2) unstable; urgency=low

  * New Maintainer, closes: Bug#94037
  * debian/control:
    - fixed description, closes: Bug#125412
    - Build-Depends: fixed, closes: Bug#94319, Bug#96040
  * mark /etc/emacs/site-start.d/50t-code.el as conffile, closes: Bug#132195

 -- NOSHIRO Shigeo <noshiro@debian.org>  Mon,  4 Mar 2002 09:34:48 +0900

t-code (2:2.2.1-1) unstable; urgency=low

  * NMU
  * New upstream version

 -- NOSHIRO Shigeo <noshiro@debian.org>  Wed, 20 Feb 2002 18:12:59 +0900

t-code (2:2.2-0.1) unstable; urgency=low

  * new upstream version.

 -- akira yamada <akira@debian.org>  Fri,  8 Feb 2002 19:10:13 +0900

t-code (2:2.1-1.1) unstable; urgency=low

  * NMU
  * Corrected the build dependencies. (closes: #94319, #96040)

 -- Adrian Bunk <bunk@fs.tum.de>  Wed, 27 Jun 2001 21:49:41 +0200

t-code (2:2.1-1) unstable; urgency=low

  * New upstream release

 -- UEYAMA Rui <rui@t-code.org>  Tue, 17 Apr 2001 15:45:49 +0900

t-code (1:2.1pre3-1) unstable; urgency=low

  * New upstream version.

 -- UEYAMA Rui <rui@debian.org>  Mon, 18 Dec 2000 15:17:53 +0900

t-code (1:2.1pre2-2) unstable; urgency=low

  * Some inernal commands are moved from /usr/bin to
    /usr/lib/t-code.
  * Applied patch for eelll.el. ([tcode-ml:1809])

 -- UEYAMA Rui <rui@debian.org>  Thu, 14 Dec 2000 19:58:03 +0900

t-code (1:2.1pre2-1) unstable; urgency=low

  * New upstream version.
  * Don't set Japanese language environment in `tc-setup.el'.
    (closes: Bug#79442)

 -- UEYAMA Rui <rui@debian.org>  Wed, 13 Dec 2000 19:26:27 +0900

t-code (1:2.0beta10-2) unstable; urgency=low

  * Renamed /usr/bin/{combine,reduce} to /usr/bin/tc-{combine,reduce}
    because of name conflicts with imagemagick commands.

 -- UEYAMA Rui <rui@debian.org>  Tue, 19 Sep 2000 07:45:08 +0900

t-code (1:2.0beta10-1) unstable; urgency=low

  * New upstream version.

 -- UEYAMA Rui <rui@debian.org>  Tue, 12 Sep 2000 08:52:08 +0900

t-code (1:2.0beta9-1) frozen unstable; urgency=low

  * New upstream version. (closes: Bug#63714, Bug#49798)
  * Don't set default-input-method for people who use other input
    methods too. (closes: Bug#64105)
  * Fixed typo in control.

 -- UEYAMA Rui <rui@linux.or.jp>  Mon, 15 May 2000 22:47:03 +0900

t-code (1:2.0beta7-1) frozen unstable; urgency=low

  * New upstream version.
  * Enabled incremental search on XEmacs21.

 -- UEYAMA Rui <rui@linux.or.jp>  Mon, 28 Feb 2000 03:23:10 +0900

t-code (1:2.0beta6c-2) frozen; urgency=low

  * Don't replace isearch-* function on XEmacs21, closes: bug #57042.

 -- UEYAMA Rui <rui@linux.or.jp>  Tue, 17 Feb 2000 09:55:05 +0900

t-code (1:2.0beta6c-1) unstable; urgency=low

  * New upstream version.

 -- UEYAMA Rui <rui@linux.or.jp>  Tue, 15 Feb 2000 21:36:52 +0900

t-code (1:2.0beta5-1) unstable; urgency=low

  * README.debian now written in English.

 -- UEYAMA Rui <rui@debian.or.jp>  Fri, 29 Oct 1999 06:07:55 +0900

t-code (2.0beta5-2) unstable; urgency=low

  * Installed bushu dictionaries to /usr/lib/t-code/.
  * Set tcode-data-directory to /usr/lib/t-code/.
  * Modified tc-{help,bushu}.el to not write files to /usr/lib/t-code/.
  * Cleaned up the debian.rules.

 -- Ueyama Rui <rui@linux.or.jp>  Thu,  3 Sep 1998 17:39:50 +0900

t-code (2.0beta5-1) unstable; urgency=low

  * Initial Release.

 -- Ueyama Rui <rui@linux.or.jp>  Tue,  1 Sep 1998 02:19:17 +0900
